const button = document.querySelector(".header__nav--active");
const menu = document.querySelector(".header__menu");
const menuClose = document.querySelector(".header__menu-close");

[button, menuClose].forEach(item => {
 return item.addEventListener("click", (e) => {
   if (!menu.classList.contains("active")) {
      menu.classList.remove("inactive");
      menu.classList.add("active");
   } else {
     menu.classList.remove("active");
     menu.classList.add("inactive");
   }
   console.log(menu.classList);
 });
})
